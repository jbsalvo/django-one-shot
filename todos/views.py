from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from todos.models import TodoList

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    # context_object_name = "whatever you want"

    # remove after deploy!!
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     print(context)
    #     return context


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


# added new
class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})
